//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.OperatingSystem
import spock.lang.Unroll

import static org.ysb33r.gradle.doxygen.DoxygenPlugin.DOX_TASK_NAME

class DoxygenTaskSpec extends spock.lang.Specification {

    public static final OperatingSystem OS = OperatingSystem.current()

    Project project
    Doxygen dox

    void setup() {
        project = ProjectBuilder.builder().build()
        project.pluginManager.apply(DoxygenPlugin)

        dox = project.tasks.getByName(DOX_TASK_NAME)
    }

    @Unroll
    void "Setting #doxName as Doxygen property that takes a boolean value"() {
        given:
        dox.options quiet: true,
            warnings: false,
            recursive: true,
            subgrouping: false

        expect:
        dox.doxygenProperties[doxName] == doxValue

        where:
        doxName       | doxValue
        'QUIET'       | 'YES'
        'WARNINGS'    | 'NO'
        'RECURSIVE'   | 'YES'
        'SUBGROUPING' | 'NO'
    }

    void "Using 'input' should throw an exception"() {
        when:
        dox.options input: '/this/path'

        then:
        thrown(DoxygenException)
    }

    void "Using 'mscgen_path' should throw an exception"() {
        when:
        dox.options mscgen_path: '/this/path'

        then:
        thrown(DoxygenException)
    }

    void "Must be able to set executable paths via executables closure"() {
        when:
        dox.executables {exec ->
            exec.identity {
                doxygen {
                    it.executableByPath('/path/to/doxygen')
                }
                mscgen {
                    it.executableByPath('/path/to/mscgen')
                }
            }
        }

        then:
        dox.doxygenLocation == (OS.windows ? project.file('/path/to/doxygen').toString() : '/path/to/doxygen')
    }

    @Unroll
    void "Lower case equivalents of Doxygen property #doxName should update final property"() {
        when:
        dox.options output_language: 'English',
            tab_size: 2,
            inherit_docs: true,
            separate_member_pages: false,
            project_logo: new File('src/resources/logo.png'),
            file_patterns: ['*.c', '*.cpp'],
            project_brief: 'This is a description with spaces'

        then:
        dox.doxygenProperties[doxName] == doxValue

        where:
        doxName                 | doxValue
        'OUTPUT_LANGUAGE'       | 'English'
        'TAB_SIZE'              | '2'
        'INHERIT_DOCS'          | 'YES'
        'SEPARATE_MEMBER_PAGES' | 'NO'
        'FILE_PATTERNS'         | '*.c *.cpp'
        'PROJECT_BRIEF'         | '"This is a description with spaces"'
    }
}

