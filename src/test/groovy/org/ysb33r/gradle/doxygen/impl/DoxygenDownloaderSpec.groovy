//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen.impl

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.doxygen.testfixtures.DownloadTestSpecification
import org.ysb33r.grolifant.api.core.ProjectOperations
import spock.lang.IgnoreIf
import spock.util.environment.RestoreSystemProperties

class DoxygenDownloaderSpec extends DownloadTestSpecification {

    Project project = ProjectBuilder.builder().build()
    ProjectOperations projectOperations

    void setup() {
        projectOperations = ProjectOperations.maybeCreateExtension(project)
    }

    @IgnoreIf({ DownloadTestSpecification.SKIP_TESTS || !DoxygenDistributionDownloader.DOWNLOAD_SUPPORTED })
    @RestoreSystemProperties
    void "Download a Doxygen executable"() {
        given: "A requirement to download Doxygen #DOX_VERSION"
        System.setProperty(
            'org.ysb33r.gradle.doxygen.download.url',
            projectOperations.stringTools.urize(DOWNLOAD_CACHE_DIR).toString()
        )
        DoxygenDistributionDownloader dwn = new DoxygenDistributionDownloader(projectOperations)
        dwn.downloadRoot = new File(project.buildDir, 'download')

        when: "The distribution root is requested"
        File dox = dwn.getDoxygenExecutablePath(DOX_VERSION).get()

        then: "The distribution is downloaded and unpacked"
        dox.exists()

        when: "The doxygen executable is run to display the help page"
        projectOperations.exec {spec ->
            spec.identity {
                executable dox.absolutePath
                args '-h'
            }
        }

        then: "No runtime error is expected"
        noExceptionThrown()
    }
}