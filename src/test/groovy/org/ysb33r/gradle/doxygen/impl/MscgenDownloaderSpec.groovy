//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen.impl

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.doxygen.testfixtures.DownloadTestSpecification
import org.ysb33r.grolifant.api.core.ProjectOperations
import spock.lang.IgnoreIf
import spock.util.environment.RestoreSystemProperties

@IgnoreIf({ DownloadTestSpecification.SKIP_TESTS || !MscgenDistributionDownloader.DOWNLOAD_SUPPORTED })
class MscgenDownloaderSpec extends DownloadTestSpecification {

    Project project = ProjectBuilder.builder().build()
    ProjectOperations projectOperations

    void setup() {
        projectOperations = ProjectOperations.maybeCreateExtension(project)
    }

    @RestoreSystemProperties
    void "Download a Mscgen executable"() {
        given:
        System.setProperty(
            'org.ysb33r.gradle.mscgen.download.url',
            projectOperations.stringTools.urize(DOWNLOAD_CACHE_DIR).toString()
        )
        MscgenDistributionDownloader dwn = new MscgenDistributionDownloader(projectOperations)
        dwn.downloadRoot = new File(project.buildDir, 'download')

        when: "The distribution root is requested"
        File mscgen = dwn.getMscgenExecutablePath(MSCGEN_VERSION).get()

        then: "The distribution is downloaded and unpacked"
        mscgen.exists()

        when: "The mscgen executable is run to display the help page"
        projectOperations.exec {spec ->
            spec.identity {
                executable mscgen.absolutePath
                args '-l'
            }
        }

        then: "No runtime error is expected"
        noExceptionThrown()
    }
}