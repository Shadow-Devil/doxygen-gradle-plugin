//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen.impl

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.downloader.AbstractDistributionInstaller
import org.ysb33r.grolifant.api.core.downloader.ExecutableDownloader

/**
 * Base class for downloading distributions.
 *
 * @since 1.0
 */
@CompileStatic
abstract class BaseDistributionInstaller extends AbstractDistributionInstaller implements ExecutableDownloader {

    final String uriPropertyName
    final Provider<String> baseURI

    protected BaseDistributionInstaller(
        final String name,
        final String relPath,
        final String baseUrl,
        final String propShortName,
        ProjectOperations po
    ) {
        super(name, relPath, po)
        this.uriPropertyName = "org.ysb33r.gradle.${propShortName}.download.url"
        this.baseURI = po.resolveProperty(uriPropertyName, baseUrl)
    }
}
