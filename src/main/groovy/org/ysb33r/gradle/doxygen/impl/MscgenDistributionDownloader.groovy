//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen.impl

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations

import static org.ysb33r.grolifant.api.core.OperatingSystem.Arch.X86
import static org.ysb33r.grolifant.api.core.OperatingSystem.Arch.X86_64

/**
 *  Downloads specific versions of {@code mscgen}.
 *
 * Currently limited to Linux &  Windows x86 architectures as these are the only ones for which
 * binary packages are available from the Mscgen site.
 *
 * @since 1.0
 */
@CompileStatic
class MscgenDistributionDownloader extends BaseDistributionInstaller {
    public static final OperatingSystem OS = OperatingSystem.current()
    public static final boolean DOWNLOAD_SUPPORTED = (OS.linux && OS.arch in [X86, X86_64]) || (OS.windows)

    MscgenDistributionDownloader(final ProjectOperations projectOperations) {
        super(
            'mscgen',
            'native-binaries/mscgen',
            'https://www.mcternan.me.uk/mscgen/software',
            'doxygen',
            projectOperations
        )
    }

    /** Provides an appropriate URI to download a specific version of Mscgen.
     *
     * @param ver Version of Mscgen to download
     * @return URI for Linux & Windows. {@code null} otherwise
     */
    @Override
    URI uriFromVersion(final String ver) {
        if (OS.isWindows()) {
            "${baseURI.get()}/mscgen-w32-${ver}.zip".toURI()
        } else if (OS.isLinux()) {
            "${baseURI.get()}/mscgen-static-${ver}.tar.gz".toURI()
        } else {
            null
        }
    }

    @Override
    File getByVersion(String version) {
        getMscgenExecutablePath(version).get()
    }

    /**
     * Returns the path to the {@code mscgen} executable.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code mscgen} or null if not a supported operating system.
     */
    Provider<File> getMscgenExecutablePath(String version) {
        if (OS.isWindows()) {
            getDistributionFile(version, 'bin/mscgen.exe')
        } else if (OS.isLinux()) {
            getDistributionFile(version, 'bin/mscgen')
        } else {
            null
        }
    }
}

