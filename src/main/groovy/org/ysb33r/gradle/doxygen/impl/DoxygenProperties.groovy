//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen.impl

import groovy.transform.CompileStatic
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.doxygen.DoxygenException
import org.ysb33r.grolifant.api.core.ProjectOperations

import java.nio.file.Path
import java.util.regex.Pattern

/**
 * Created by schalkc on 21/05/2014.
 */
@CompileStatic
class DoxygenProperties {

    static String escapeValue(final String s) {
        s =~ CONTAINS_SPACES ? "\"${s}\"" : s
    }

    static String escapeAndJoinFileCollection(final FileCollection fc) {
        Transform.toSet(fc.files) {
            escapeValue(it.absolutePath)
        }.join(' ')
    }

    DoxygenProperties(ProjectOperations po) {
        projectOperations = po
    }

    void option(final String name, Object value) {
        setProvider(name, asEscapedScalarProvider(value))
    }

    void listOption(final String name, Iterable<?> value) {
        listOption(name, value.toList())
    }

    void listOption(final String name, Collection<?> value) {
        if (value instanceof FileCollection) {
            setProvider(name, projectOperations.provider { ->
                escapeAndJoinFileCollection((FileCollection) value)
            })
        } else {
            final providers = Transform.toList(value.asList()) {
                asEscapedScalarProvider(it)
            }
            setProvider(name, projectOperations.provider { ->
                providers*.get().join(' ')
            })
        }
    }

    Map<String, String> getOptions() {
        doxyUpdate.collectEntries { k, provider ->
            [k, provider.get()]
        } as Map<String, String>
    }

    private void setProvider(final String name, Provider<String> value) {
        doxyUpdate[doxName(name)] = value
    }

    private Provider<String> asEscapedScalarProvider(Object value) {
        asScalarProvider(value).map {
            escapeValue(projectOperations.stringTools.stringize(it))
        }
    }

    private Provider<?> asScalarProvider(Object value) {
        switch (value) {
            case Provider:
                (Provider) value
                break
            case Number:
            case CharSequence:
                projectOperations.provider { -> value.toString() }
                break
            case File:
                projectOperations.provider { ->
                    projectOperations.fsOperations.file(value).absolutePath
                }
                break
            case Path:
                asScalarProvider(((Path) value).toFile())
                break
            case Boolean:
                asScalarProvider(((Boolean) value) ? 'YES' : 'NO')
                break
            default:
                throw new DoxygenException("${value.class.canonicalName} is not an accaptable type")
        }
    }

    private String doxName(final String name) {
        name.toUpperCase()
    }

    private final Map<String, Provider<String>> doxyUpdate = [:]
    private final ProjectOperations projectOperations
    private final static Pattern CONTAINS_SPACES = ~/\s+/
}
