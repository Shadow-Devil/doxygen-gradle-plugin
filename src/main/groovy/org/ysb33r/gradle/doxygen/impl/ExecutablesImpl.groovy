//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen.impl

import groovy.transform.CompileStatic
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType
import org.gradle.api.Action
import org.gradle.api.Task
import org.ysb33r.gradle.doxygen.DotExecutable
import org.ysb33r.gradle.doxygen.DoxygenExecutable
import org.ysb33r.gradle.doxygen.Executables
import org.ysb33r.gradle.doxygen.MscgenExecutable
import org.ysb33r.gradle.doxygen.OptionalBaseToolLocation
import org.ysb33r.gradle.doxygen.OptionalToolLocation
import org.ysb33r.gradle.doxygen.SystemExecutable
import org.ysb33r.grolifant.api.core.ClosureUtils
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.runnable.ToolLocation

import static org.ysb33r.grolifant.api.core.ClosureUtils.configureItem
import static org.ysb33r.grolifant.api.core.ResourceUtils.loadPropertiesFromResource

/**
 *
 * Implementation for configuring executables.
 *
 * @since 1.0
 */
@CompileStatic
class ExecutablesImpl implements Executables {

    ExecutablesImpl(Task task, ProjectOperations po) {
        Properties props = loadPropertiesFromResource('META-INF/doxygen-gradle/doxygen.properties')
        this.projectOperations = po

        this.doxygenExecutable = new DoxygenExecutable(task, po)
        if (DoxygenDistributionDownloader.DOWNLOAD_SUPPORTED) {
            this.doxygenExecutable.executableByVersion(props['doxygen'])
        }

        this.dotExecutable = new DotExecutable(task, po)
        if (DotDistributionDownloader.DOWNLOAD_SUPPORTED) {
            this.dotExecutable.executableByVersion(props['graphviz'])
        }

        this.mscgenExecutable = new MscgenExecutable(task, po)
        if (MscgenDistributionDownloader.DOWNLOAD_SUPPORTED) {
            this.mscgenExecutable.executableByVersion(props['mscgen'])
        }

        this.perlExecutable = new SystemExecutable(task, po)
        this.perlExecutable.executableBySearchPath('perl')

        this.hhcExecutable = new SystemExecutable(task, po)
        this.hhcExecutable.executableBySearchPath('chm')
    }

    @Override
    void doxygen(Action<ToolLocation> configurator) {
        configurator.execute(this.doxygenExecutable)
    }

    @Override
    void doxygen(@ClosureParams(value = SimpleType, options = "org.ysb33r.grolifant.api.core.runnable.ToolLocation") Closure configurator) {
        configureItem(this.doxygenExecutable,configurator)
    }

    DoxygenExecutable getDoxygen() {
        this.doxygenExecutable
    }

    @Override
    void dot(Action<OptionalToolLocation> configurator) {
        configurator.execute(this.dotExecutable)
    }

    @Override
    void dot(@ClosureParams(value = SimpleType, options = "org.ysb33r.gradle.doxygen.OptionalToolLocation") Closure configurator) {
        configureItem(this.dotExecutable,configurator)
    }

    @Override
    void hhc(Action<OptionalBaseToolLocation> configurator) {
        configurator.execute(this.hhcExecutable)
    }

    @Override
    void hhc(@ClosureParams(value = SimpleType, options = "org.ysb33r.gradle.doxygen.OptionalBaseToolLocation") Closure configurator) {
        configureItem(this.hhcExecutable,configurator)
    }

    @Override
    void mscgen(Action<OptionalToolLocation> configurator) {
        configurator.execute(this.mscgenExecutable)
    }

    @Override
    void mscgen(@ClosureParams(value = SimpleType, options = "org.ysb33r.gradle.doxygen.OptionalBaseToolLocation") Closure configurator) {
        configureItem(this.mscgenExecutable,configurator)
    }

    @Override
    void perl(Action<OptionalBaseToolLocation> configurator) {
        configurator.execute(this.perlExecutable)
    }

    @Override
    void perl(@ClosureParams(value = SimpleType, options = "org.ysb33r.gradle.doxygen.OptionalBaseToolLocation") Closure configurator) {
        configureItem(this.perlExecutable,configurator)
    }

    Map<String, String> getOtherExecutables() {
        Map<String, String> exes = [:]

        if (this.dotExecutable.enabled) {
            exes.put('DOT_PATH', this.dotExecutable.executable.get().absolutePath)
        }
        if (this.mscgenExecutable.enabled) {
            exes.put('MSCGEN_PATH', this.mscgenExecutable.executable.get().absolutePath)
        }
        if (this.perlExecutable.enabled) {
            exes.put('PERL_PATH', this.perlExecutable.executable.get().absolutePath)
        }
        if (this.hhcExecutable.enabled) {
            exes.put('HHC_LOCATION', this.hhcExecutable.executable.get().absolutePath)
        }
        exes
    }

    private final ProjectOperations projectOperations
    private final DoxygenExecutable doxygenExecutable
    private final DotExecutable dotExecutable
    private final MscgenExecutable mscgenExecutable
    private final SystemExecutable perlExecutable
    private final SystemExecutable hhcExecutable
}
