//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SourceTask
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.doxygen.impl.DoxyfileEditor
import org.ysb33r.gradle.doxygen.impl.DoxygenProperties
import org.ysb33r.gradle.doxygen.impl.ExecutablesImpl
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.internal.core.IterableUtils

import java.util.regex.Pattern

import static org.ysb33r.gradle.doxygen.impl.DoxygenProperties.escapeAndJoinFileCollection
import static org.ysb33r.gradle.doxygen.impl.DoxygenProperties.escapeValue

/**
 * A wrapper task for calling GNU Make. This is useful for migrating legacy builds
 * or when complex build need to construct components that use GNU Make as a
 * build tool.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class Doxygen extends SourceTask {
    /** Constructs a Doxygen task object and sets some default Doxygen properties
     *
     */
    Doxygen() {
        this.projectOperations = ProjectOperations.find(project)
        this.executableLocations = new ExecutablesImpl(this, projectOperations)
        this.doxyUpdate = new DoxygenProperties(projectOperations)
        this.doxyTempFile = projectOperations.buildDirDescendant("tmp/${name}.doxyfile")
        this.outputDir = projectOperations.providerTools.property(
            File,
            projectOperations.buildDirDescendant('docs/doxygen')
        )

        doxyUpdate.option('PROJECT_NAME', projectOperations.projectName)
        doxyUpdate.option('QUIET', project.logger.isQuietEnabled())
        doxyUpdate.option('WARNINGS', !project.logger.isQuietEnabled())
        doxyUpdate.option('PROJECT_NUMBER', projectOperations.projectTools.versionProvider)
    }

    /** Docuemtnation output directory
     *
     * @return Directory that was set earlier via {@link #setOutputDir} otherwise {@code $buildDir/docs/doxygen}   .
     */
    @OutputDirectory
    Provider<File> getOutputDir() {
        this.outputDir
    }

    /** Sets the output directory
     *
     * @param dir A path that can be converetd with {@code project.file}.
     */
    void setOutputDir(Object dir) {
        projectOperations.fsOperations.updateFileProperty(this.outputDir, dir)
    }

    /** Allows for setting paths to various executables.
     *
     * <p>
     * By default specific versions of {@code doxygen} and {@code dot} will be downloaded.
     * {@code mscgen}
     * </p>
     *
     * @param cfg A configuration closure
     *
     */
    void executables(Action<Executables> cfg) {
        cfg.execute(this.executableLocations)
    }

    @Optional
    @InputFile
    File getTemplate() {
        this.templateFile
    }

    @InputFiles
    FileCollection getImagePaths() {
        projectOperations.fsOperations.files(imagePaths)
    }

    /** Sets the template Doxyfile to be used. If not supplied a default one will be
     * generated to be used as a template.
     *
     * @param tmpl Template Doxyfile
     */
    void template(final File tmpl) {
        templateFile = tmpl
    }

    /** Sets the template Doxyfile to be used. If not supplied a default one will be
     * generated to be used as a template.
     *
     * @param tmpl Template Doxyfile
     */
    void template(final String tmpl) {
        template project.file(tmpl)
    }

    /** Allows for one more image paths to be set
     *
     * @param paths
     */
    void image_paths(final Object... paths) {
        this.imagePaths.addAll(paths)
    }

    /** Alias for outputDir
     *
     * @param outdir
     */
    void output_directory(Object outdir) {
        projectOperations.fsOperations.updateFileProperty(this.outputDir, outdir)
    }

    /**
     * Provides a single Doxygen property.
     *
     * @param key Name opf Doxugen property
     * @param value Value of property
     */
    void option(String key, Object value) {
        switch (key) {
            case 'aliases':
            case 'allexternals':
            case 'exclude':
            case 'predefined':
            case 'quiet':
            case 'recursive':
            case 'searchengine':
            case 'subgrouping':
            case 'warnings':
                doxyUpdate.option(key, value)
                break

            case 'input':
                throw new DoxygenException("'${key}' is ignored, use 'source' and 'sourceDir' instead (with exclude patterns as appropriate).")
                break

            case 'mscgen_path':
            case 'dot_path':
            case 'perl_path':
            case 'hhc_location':
                throw new DoxygenException("'${key}' is ignored, use 'executables' instead.")
                break

            default:
                if (key.find(/.+_.+/) && VALID_PROPERTY_NAME.matcher(key).matches()) {
                    if (value instanceof Collection) {
                        doxyUpdate.listOption(key, (Collection) value)
                    } else if (IterableUtils.treatAsIterable(value)) {
                        doxyUpdate.listOption(key, (Iterable) value)
                    } else {
                        doxyUpdate.option(key, value)
                    }
                } else {
                    throw new DoxygenException("'${key}' is not a valid configuration option")
                }
        }
    }

    /**
     * Provide many Doxygen properties via a map.
     *
     * @param settings Map of settings
     */
    void options(Map<String, Object> settings) {
        settings.each { key, value ->
            option(key, value)
        }
    }

    /**
     * Returns the current hashmap of Doxygen properties that will override settings in the Doxygen file
     */
    @Input
    Map<String, String> getDoxygenProperties() {
        final baseOptions = doxyUpdate.options

        if (imagePaths.size()) {
            baseOptions.put('IMAGE_PATH', escapeAndJoinFileCollection(getImagePaths()))
        }

        baseOptions.put('INPUT', escapeAndJoinFileCollection(super.source))
        baseOptions.put('OUTPUT_DIRECTORY', escapeValue(outputDir.get().absolutePath))
        executableLocations.otherExecutables.each { doxProp, location ->
            baseOptions.put(doxProp, escapeValue(location))
        }

        baseOptions.asImmutable()
    }

    @Internal
    String getDoxygenLocation() {
        executableLocations.doxygen.executable.get().absolutePath
    }

    @TaskAction
    void exec() {
        final dox = doxygenLocation
        File doxyfile = createDoxyfile(dox)
        logger.debug "Using ${doxyfile} as Doxygen configuration file"
        editDoxyfile(doxyfile)
        runDoxygen(dox, projectOperations.projectDir, [doxyfile.absolutePath])
    }


    /** Creates a Doxyfile that will eventually be passed to the Doxygen executable.
     * If a template has been set, if will make a copy of that, otherwise it will call
     * Doxygen to generate a default file.
     *
     * @return The Doxyfile File instance.
     */
    private File createDoxyfile(String doxPath) {
        File doxyfile = doxyTempFile.get()
        doxyfile.parentFile.mkdirs()
        if (templateFile) {
            if (!templateFile.exists()) {
                throw new DoxygenException("${templateFile} does not exist")
            }
            doxyfile.text = templateFile.text
        } else {
            runDoxygen(doxPath, doxyfile.parentFile, ['-g', doxyfile.name])
        }

        return doxyfile
    }

    /** Edits a Doxyfile and replaces existing properties with ones passed down via
     * Gradle configuration.
     *
     * @param doxyfile
     */
    private void editDoxyfile(File doxyfile) {
        DoxyfileEditor editor = new DoxyfileEditor()
        editor.update(doxygenProperties, doxyfile)
    }

    /** Runs the Doxygen executable
     *
     * @param doxyfile
     * @param cmdargs
     */
    private void runDoxygen(final String doxPath, File doxWorkdir, Collection<String> cmdargs = []) {
        projectOperations.exec { spec ->
            spec.identity {
                executable = doxPath
                workingDir = doxWorkdir
                args(cmdargs)
            }
        }
    }

    private File templateFile = null

    private List<Object> imagePaths = []
    private final Property<File> outputDir
    private final DoxygenProperties doxyUpdate
    private final ExecutablesImpl executableLocations
    private final ProjectOperations projectOperations
    private final Provider<File> doxyTempFile
    private static final Pattern VALID_PROPERTY_NAME = ~/[_\p{Digit}\p{Lower}]{3,}/
}



