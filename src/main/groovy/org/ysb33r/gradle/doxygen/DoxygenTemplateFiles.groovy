//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant.api.core.ProjectOperations

import javax.inject.Inject

/**
 * Allows for the creation of template files in a location. 'src/doxygen' is the default.
 */
@CompileStatic
class DoxygenTemplateFiles extends DefaultTask {

    @Inject
    DoxygenTemplateFiles(Provider<Doxygen> doxygenProvider) {
        this.projectOperations = ProjectOperations.find(project)
        this.outputDirectory = projectOperations.providerTools.property(File)
        this.outputDirectory.set(new File(project.projectDir, 'src/doxygen'))
        this.prefix = project.name
        this.doxygenLocation = doxygenProvider.map {
            it.doxygenLocation
        }
    }

    /**
     * Location where to generate files into. By default this will be src/doxygen.
     *
     * @param dir Anything convertaible to a {@code File}.
     */
    void setOutputDirectory(Object dir) {
        projectOperations.fsOperations.updateFileProperty(outputDirectory, dir)
    }

    /**
     * Prefix used for naming files. By default it is the name of the project.
     *
     * @param prefix project prefix
     */
    void setPrefix(String prefix) {
        this.prefix = prefix
    }

    @TaskAction
    void exec() {
        final File location = outputDirectory.get()
        location.mkdirs()
        runDoxygen '-l', new File(location, prefix + 'LayoutTemplate.xml').absolutePath
        runDoxygen '-w', 'rtf', new File(location, prefix + 'Style.rtf').absolutePath
        runDoxygen '-w', 'html', new File(location, prefix + 'Header.html').absolutePath,
            new File(location, prefix + 'Footer.html').absolutePath,
            new File(location, prefix + '.css').absolutePath
        runDoxygen '-w', 'latex', new File(location, prefix + 'Header.tex').absolutePath,
            new File(location, prefix + 'Footer.tex').absolutePath,
            new File(location, prefix + 'Style.tex').absolutePath
        runDoxygen '-e', 'rtf', new File(location, prefix + 'Extensions.rtf').absolutePath
    }

    /** Runs the Doxygen executable
     *
     * @param cmdargs
     */
    private void runDoxygen(String... cmdargs) {
        projectOperations.exec { spec ->
            spec.identity {
                executable = doxygenLocation.get()
                args(cmdargs)
            }
        }
    }

    private String prefix
    private final Property<File> outputDirectory
    private final ProjectOperations projectOperations
    private final Provider<String> doxygenLocation
}
