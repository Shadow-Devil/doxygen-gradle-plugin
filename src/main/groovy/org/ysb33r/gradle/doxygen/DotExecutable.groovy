//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.tasks.Internal
import org.ysb33r.gradle.doxygen.impl.DoxygenDistributionDownloader
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.downloader.ExecutableDownloader
import org.ysb33r.grolifant.api.core.runnable.AbstractToolExtension
import org.ysb33r.grolifant.api.errors.ConfigurationException

/**
 * Location of {@code dot} executable from Graphviz.
 *
 * Default behaviour is to down a Graphviz package on Windows, but used the installed version on other platforms.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.0
 */
@CompileStatic
class DotExecutable extends AbstractToolExtension<DotExecutable> implements OptionalToolLocation {
    DotExecutable(Task task, ProjectOperations projectOperations) {
        super(task, projectOperations, null)
        this.downloader = new DoxygenDistributionDownloader(projectOperations)
    }

    @Internal
    boolean enabled = false

    @Override
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        File exec = executablePathOrNull() ?: executableSearchPathOrNull()
        if (!exec) {
            return null
        }
        try {
            projectOperations.execTools.parseVersionFromOutput(
                ['-V'],
                exec,
                { String output ->
                    final versionLine = output.readLines()[0]
                    versionLine.split(output.readLines()[0])[4]
                }
            )
        } catch (RuntimeException e) {
            throw new ConfigurationException('Cannot determine version', e)
        }
    }

    @Override
    protected ExecutableDownloader getDownloader() {
        this.downloader
    }

    private final DoxygenDistributionDownloader downloader
}
