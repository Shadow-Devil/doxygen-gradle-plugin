//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.tasks.Internal
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.runnable.AbstractBaseToolExtension
import org.ysb33r.grolifant.api.errors.ConfigurationException

@CompileStatic
class SystemExecutable extends AbstractBaseToolExtension<SystemExecutable> implements OptionalBaseToolLocation {

    @Internal
    boolean enabled = false

    SystemExecutable(Task task, ProjectOperations projectOperations) {
        super(task, projectOperations, null)
    }

    @Override
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        return null
    }
}
